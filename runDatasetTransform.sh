#!/bin/bash

for i in $(ls testsets/$1/ | sed 's/\.mat$//');
        do python3 waveletTransform.py --dataset $1 --image $i;
	python3 createGridWavelet.py --dataset $1 --image $i;	
done;

