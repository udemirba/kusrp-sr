out_folder = 'C:\Users\demir\Documents\Umut\2023 Summer\KUSRP\DIV2K_Valid_AllInOne';

for i = 801:900

    % Read images
    im_gt = imread("DIV2K_Valid_HR\0" + i + ".png");
    im_l = imread("DIV2K_Valid_LR\0" + i + "x4.png");

    % Convert them to double
    %im_gt = im2double(HR);
    %im_l = im2double(LR);

    % Bicubic Interpolation for Low-Res Image
    im_b = imresize(im_l,4);

    % Y channels for the images
    im_gt_y = rgb2ycbcr(im_gt);
    im_l_y = rgb2ycbcr(im_l);
    im_b_y = rgb2ycbcr(im_b);

    % Create the base file name from the number
    baseFileName = sprintf('%3.3d.mat', i);
    % Prepend the folder to get the full file name.
    fullFileName = fullfile(out_folder, baseFileName);
    % Save the variable
    save(fullFileName, "im_b","im_b_y","im_gt","im_gt_y","im_l","im_l_y");
end