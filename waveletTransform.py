import numpy as np
import matplotlib.pyplot as plt

import pywt
import pywt.data

from PIL import Image
import argparse, os


parser = argparse.ArgumentParser(description="SRResNet 2D Wavelet Transform")
parser.add_argument("--image", default="801", type=str, help="image name")
parser.add_argument("--dataset", default="DIV2K", type=str, help="dataset name")

args = parser.parse_args()


# Open the image
image = Image.open('results/'+args.dataset+'/'+args.image+'Res.png')

# Convert the image to grayscale
original = image.convert('L')

# Wavelet transform of image, and plot approximation and details
titles = ['Approximation', ' Horizontal detail',
          'Vertical detail', 'Diagonal detail']
coeffs2 = pywt.dwt2(original, 'haar')
LL, (LH, HL, HH) = coeffs2

wavelet_comps = ['LL','LH','HL','HH']

fig = plt.figure(figsize=(12, 3))
for i, a in enumerate([LL, LH, HL, HH]):
    ax = fig.add_subplot(2, 2, i + 1)
    ax.imshow(a, interpolation="nearest", cmap=plt.cm.gray)
    ax.set_title(titles[i], fontsize=10)
    ax.set_xticks([])
    ax.set_yticks([])
    
    plt.imsave(f"results/Wavelets/{args.dataset}/{args.image}_{wavelet_comps[i]}.png", a, cmap=plt.cm.gray)
    

fig.tight_layout()
#plt.show()

print("Dataset=",args.dataset)
print("Image=",args.image)
print("The wavelet transform completed.\n")







