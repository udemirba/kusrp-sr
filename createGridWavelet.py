from PIL import Image
import argparse, os


parser = argparse.ArgumentParser(description="SRResNet 2D Wavelet Transform")
parser.add_argument("--image", default="801", type=str, help="image name")
parser.add_argument("--dataset", default="DIV2K", type=str, help="dataset name")

args = parser.parse_args()

# Open the four grayscale images
image1 = Image.open('results/Wavelets/'+args.dataset+'/'+args.image+'_LL.png')
image2 = Image.open('results/Wavelets/'+args.dataset+'/'+args.image+'_LH.png')
image3 = Image.open('results/Wavelets/'+args.dataset+'/'+args.image+'_HL.png')
image4 = Image.open('results/Wavelets/'+args.dataset+'/'+args.image+'_HH.png')


# Create a new blank grayscale image for the grid
grid_image = Image.new('L', (image1.width * 2, image1.height * 2))

# Paste the grayscale images onto the grid image
grid_image.paste(image1, (0, 0))
grid_image.paste(image2, (image1.width, 0))
grid_image.paste(image3, (0, image1.height))
grid_image.paste(image4, (image1.width, image1.height))

# Save the grid image
grid_image.save('results/Wavelets/'+args.dataset+'/'+args.image+'_GRID.png')

