#!/bin/bash

for i in $(ls testsets/$1/ | sed 's/\.mat$//');
	do python3 demo.py --model model/model_srresnet.pth --dataset $1 --image $i --scale 4; 
done;
